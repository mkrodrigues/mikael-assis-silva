<?php
include_once('includes/header.php');
?>

<main id="main-aeronaves">
    <h2 id="titulo-aeronaves">Aeronaves</h2>

    <div id="btn-container">
        <a id="btn-aeronaves" href="novoAeronave.php">Nova Aeronave</a>
    </div>

    <section id="centralizar">
        <table class="tabela">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Id Cliente</th>
                    <th>Matrícula</th>
                    <th>Modelo</th>
                    <th>Ano</th>
                    <th>Cor</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>

                <?php

                require_once('conexao.php');

                $p = mysqli_prepare($database, 'SELECT * FROM aeronave');
                mysqli_stmt_execute($p);
                $resultado = mysqli_stmt_get_result($p);

                while ($aeronave = mysqli_fetch_assoc($resultado)) {

                    ?>
                    <tr>
                        <td><?= $aeronave['id'] ?></td>
                        <td><?= $aeronave['id_cliente'] ?></td>
                        <td><?= $aeronave['matricula'] ?></td>
                        <td><?= $aeronave['modelo'] ?></td>
                        <td><?= $aeronave['ano'] ?></td>
                        <td><?= $aeronave['cor'] ?></td>
                        <td>
                            <a href="editarAeronave.php?id_aeronave=<?= $aeronave['id'] ?>">Editar</a>
                            <a href="excluirAeronave.php?id_aeronave=<?= $aeronave['id'] ?>">Excluir</a>
                        </td>
                    </tr>
                <?php
                }
                ?>

            </tbody>
        </table>
    </section>
</main>

</body>
</html>