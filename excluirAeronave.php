<?php 

session_start();
if(!$_SESSION['idUsuario']) header("Location: index.html");

require_once('conexao.php');

$id_aeronave = $_GET['id_aeronave'];

$p = mysqli_prepare($database, 'DELETE FROM aeronave WHERE id = ?');
mysqli_stmt_bind_param($p, 'i', $id_aeronave);
mysqli_stmt_execute($p);

header('location:aeronaves.php');

