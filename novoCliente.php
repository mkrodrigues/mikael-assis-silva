<?php
include_once('includes/header.php');
?>

<main id="main-nClientes">
    <h2 id="titulo-nClientes">Novo Cliente</h2>

    <form action="adicionarCliente.php" method="POST" id="centralizar">
        <input class="campo" type="text" placeholder="Nome" name="nome">
        <input class="campo" type="text" placeholder="E-mail" name="email">
        <input class="campo" type="number" placeholder="CPF" name="cpf">
        <button type="submit" id="btn-nClientes">Cadastrar Cliente</button>
    </form>

</main>

</body>
</html>