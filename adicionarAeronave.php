<?php

session_start();
if(!$_SESSION['idUsuario']) header("Location: index.html");

require_once('conexao.php');

$id_cliente = $_POST['id_cliente'];
$matricula = $_POST['matricula'];
$modelo = $_POST['modelo'];
$ano = $_POST['ano'];
$cor = $_POST['cor'];

$p = mysqli_prepare($database, 'INSERT INTO aeronave(id_cliente, matricula, modelo, ano, cor) VALUES (?, ?, ?, ?, ?)');
mysqli_stmt_bind_param($p, 'issis', $id_cliente, $matricula, $modelo, $ano, $cor);
mysqli_stmt_execute($p);

header('location:aeronaves.php');