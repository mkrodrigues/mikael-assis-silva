<?php
session_start();
if (!$_SESSION['idUsuario']) header("Location: index.html");
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/estilo.css">
    <title>Php - Nota Complementar</title>
</head>

<body>

    <header id="header">
        <a href="principal.php">
            <h1 id="logo">&lt;?PHP?&gt;</h1>
        </a>

        <div id="coluna2">
            <h2 id="nome"><?= $_SESSION['nome'] ?></h2>
            <a id="sair" href="sair.php">Sair</a>
        </div>
    </header>

    <section id="bcg-menu">
        <nav id="menu">
            <ul>
                <li><a href="clientes.php">Clientes</a></li>
                <li><a href="aeronaves.php">Aeronaves</a></li>
            </ul>
        </nav>
    </section>