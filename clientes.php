<?php
include_once('includes/header.php');
?>

<main id="main-clientes">

    <h2 id="titulo-clientes">Clientes</h2>

    <div id="btn-container">
        <a id="btn-clientes" href="novoCliente.php">Novo Cliente</a>
    </div>

    <section id="centralizar">
        <table class="tabela">
            <thead>
                <tr>
                    <th>Id Cliente</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>CPF</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>

                <?php

                require_once('conexao.php');

                $p = mysqli_prepare($database, 'SELECT * FROM cliente');
                mysqli_stmt_execute($p);
                $resultado = mysqli_stmt_get_result($p);

                while ($cliente = mysqli_fetch_assoc($resultado)) {

                    ?>

                    <tr>
                        <td><?= $cliente['id'] ?></td>
                        <td><?= $cliente['nome'] ?></td>
                        <td><?= $cliente['email'] ?></td>
                        <td><?= $cliente['cpf'] ?></td>
                        <td>
                            <a href="editarCliente.php?id_cliente=<?= $cliente['id'] ?>">Editar</a>
                            <a href="excluirCliente.php?id_cliente=<?= $cliente['id'] ?>">Excluir</a>
                        </td>

                    </tr>

                <?php
                }
                ?>

            </tbody>

        </table>
    </section>
</main>

</body>
</html>