<?php 

session_start();
if(!$_SESSION['idUsuario']) header("Location: index.html");

require_once('conexao.php');

$id_cliente = $_GET['id_cliente'];

$p = mysqli_prepare($database, 'DELETE FROM cliente WHERE id = ?');
mysqli_stmt_bind_param($p, 'i', $id_cliente);
mysqli_stmt_execute($p);

header('location:clientes.php');

