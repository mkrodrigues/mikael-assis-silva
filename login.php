<?php
session_start();
require_once('conexao.php');

$login = $_POST['login'];
$senha = $_POST['password'];

if( isset($login) && $login != ""){
	$p = mysqli_prepare( $database, 'SELECT id_usuario, nome_user, senha FROM usuario WHERE nome_user = ?');
    mysqli_stmt_bind_param($p, 's', $login);
	mysqli_stmt_execute($p);
	$resultado = mysqli_stmt_get_result($p);
    $usuario = $resultado->fetch_assoc();

    if( $senha == $usuario['senha']){
        $_SESSION['nome'] = $usuario['nome_user'];
        $_SESSION['idUsuario'] = $usuario['id_usuario'];
        header("location:principal.php");
        exit();
    }
}

header("Location: index.html");
