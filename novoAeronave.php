<?php
include_once('includes/header.php');
?>

<main id="main-nAeronaves">
    <h2 id="titulo-nAeronaves">Nova Aeronave</h2>

    <form action="adicionarAeronave.php" method="POST" id="centralizar">
        <input class="campo" type="text" placeholder="Id Cliente" name="id_cliente">
        <input class="campo" type="text" placeholder="Matrícula" name="matricula">
        <input class="campo" type="text" placeholder="Modelo" name="modelo">
        <input class="campo" type="number" placeholder="Ano" name="ano">
        <input class="campo" type="text" placeholder="Cor" name="cor">
        <button type="submit" id="btn-nAeronaves">Cadastrar Aeronave</button>
    </form>
</main>
</body>
</html>