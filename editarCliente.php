<?php

require_once('conexao.php');

$id_cliente = $_GET['id_cliente'];

if (isset($_POST["submit"])) {
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $cpf = $_POST['cpf'];
}

if (!empty($nome)) {

    $p = mysqli_prepare($database, 'UPDATE cliente SET nome = ?, email = ?, cpf = ? WHERE id = ?');

    mysqli_stmt_bind_param($p, 'ssii', $nome, $email, $cpf, $id_cliente);
    mysqli_stmt_execute($p);

    header('location:clientes.php');
}

$p = mysqli_prepare($database, 'SELECT * FROM cliente WHERE id = ?');
mysqli_stmt_bind_param($p, 's', $id_cliente);

mysqli_stmt_execute($p);

$resultado = mysqli_stmt_get_result($p);
$cliente = mysqli_fetch_assoc($resultado);

?>
<?php
require_once('includes/header.php')
?>

<main id="main-eClientes">
    <h2 id="titulo-eClientes">Editar Cliente</h2>

    <form action="editarCliente.php?id_cliente=<?= $id_cliente ?>" method="POST" id="centralizar">
        <input class="campo" type="text" placeholder="Nome" name="nome" value="<?= $cliente['nome'] ?>">
        <input class="campo" type="text" placeholder="E-mail" name="email" value="<?= $cliente['email'] ?>">
        <input class="campo" type="number" placeholder="CPF" name="cpf" value="<?= $cliente['cpf'] ?>">
        <button type="submit" name="submit" id="btn-eClientes">Atualizar Cliente</button>
    </form>

</main>
</body>

</html>