<?php
include_once('includes/header.php');
?>

<main id="main-principal">
    <h1 id="titulo-principal"> Tabela Principal</h1>

    <table id="tabela">
        <thead>
            <tr>
                <th>Id Cliente</th>
                <th>Nome Cliente</th>
                <th>E-mail</th>
                <th>CPF</th>
                <th>Id Aeronave</th>
                <th>Matrícula</th>
                <th>Modelo</th>
                <th>Ano</th>
                <th>Cor</th>
            </tr>
        </thead>

        <tbody>
            <?php

            require_once('conexao.php');

            $p = mysqli_prepare($database, 'SELECT Cliente.id, Cliente.nome, Cliente.email, Cliente.cpf, Aeronave.id_cliente, Aeronave.id, Aeronave.matricula, Aeronave.modelo, Aeronave.ano, Aeronave.cor FROM cliente INNER JOIN aeronave ON Cliente.id = Aeronave.id_cliente');
            mysqli_stmt_execute($p);

            $resultado = mysqli_stmt_get_result($p);

            while ($principal = mysqli_fetch_assoc($resultado)) {

                ?>
                <tr>
                    <td><?= $principal['id_cliente'] ?></td>
                    <td><?= $principal['nome'] ?></td>
                    <td><?= $principal['email'] ?></td>
                    <td><?= $principal['cpf'] ?></td>
                    <td><?= $principal['id'] ?></td>
                    <td><?= $principal['matricula'] ?></td>
                    <td><?= $principal['modelo'] ?></td>
                    <td><?= $principal['ano'] ?></td>
                    <td><?= $principal['cor'] ?></td>
                </tr>

            <?php
            }
            ?>


        </tbody>
    </table>
</main>
</body>
</html>