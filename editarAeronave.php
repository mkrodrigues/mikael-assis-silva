<?php

require_once('conexao.php');

$id_aeronave = $_GET['id_aeronave'];

if (isset($_POST["submit"])) {
    $id_cliente = $_POST['id_cliente'];
    $matricula = $_POST['matricula'];
    $modelo = $_POST['modelo'];
    $ano = $_POST['ano'];
    $cor = $_POST['cor'];
}

if (!empty($id_cliente)) {

    $p = mysqli_prepare($database, 'UPDATE aeronave SET id_cliente = ?, matricula = ?, modelo = ?, ano = ?, cor = ? WHERE id = ?');
    mysqli_stmt_bind_param($p, 'issisi', $id_cliente, $matricula, $modelo, $ano, $cor, $id_aeronave);
    mysqli_stmt_execute($p);
    header('location:aeronaves.php');
}

$p = mysqli_prepare($database, 'SELECT * FROM aeronave WHERE id = ?');
mysqli_stmt_bind_param($p, 's', $id_aeronave);
mysqli_stmt_execute($p);

$resultado = mysqli_stmt_get_result($p);
$aeronave = mysqli_fetch_assoc($resultado);

?>

<?php
require_once('includes/header.php')
?>

<main id="main-eAeronaves">
    <h2 id="titulo-eAeronaves">Editar Aeronave</h2>

    <form action="editarAeronave.php?id_aeronave=<?= $id_aeronave ?>" method="POST" id="centralizar">
        <input class="campo" type="text" placeholder="Id Cliente" name="id_cliente" value="<?= $aeronave['id_cliente'] ?>">
        <input class="campo" type="text" placeholder="Matrícula" name="matricula" value="<?= $aeronave['matricula'] ?>">
        <input class="campo" type="text" placeholder="Modelo" name="modelo" value="<?= $aeronave['modelo'] ?>">
        <input class="campo" type="number" placeholder="Ano" name="ano" value="<?= $aeronave['ano'] ?>">
        <input class="campo" type="text" placeholder="Cor" name="cor" value="<?= $aeronave['cor'] ?>">
        <button type="submit" name="submit" id="btn-eAeronaves">Atualizar Aeronave</button>
    </form>
</main>

</body>
</html>